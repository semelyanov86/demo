<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Auth;
use App\Models\UserExtra;

class ProductController extends Controller
{
    public function show()
    {
        $user = Auth::user();
        $extra = UserExtra::where("email", $user->email)->first();
        if ($extra) {
            $url =
                "https://" .
                $extra->api .
                ":" .
                $extra->password .
                "@" .
                $extra->store .
                ".myshopify.com/admin/api/2021-04/products.json";

            $options = [
                "http" => [
                    "method" => "GET",
                ],
            ];
            $context = stream_context_create($options);
            $result = file_get_contents($url, false, $context);

            $data = json_decode($result, true);
        } else {
            $data = [];
        }

        return Inertia::render("Product", [
            "data" => $data,
        ]);
    }
}
