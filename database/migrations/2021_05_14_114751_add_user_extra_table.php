<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserExtraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table("user_extras")->insert([
            "email" => "user@mail.com",
            "store" => "ser-samplestore",
            "api" => "7ad03de86fc7a353aead7f4d3156c8c6",
            "password" => "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",
            "created_at" => date("Y-m-d H:m:s"),
            "updated_at" => date("Y-m-d H:m:s"),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table("user_extras")
            ->where("email", "=", "user@mail.com")
            ->delete();
    }
}
